#!/usr/bin/env python

# This runs on the cloud. Gets transformations from a ros_bridge topic and publishes the necessary transforms

import time
import rospy
import tf2_ros
from geometry_msgs.msg import TransformStamped


# Base_link to Odometry Callback
def tf_BL_OD_CB(msg):
	tb=tf2_ros.TransformBroadcaster()
	#print("got a BL->odom transform.")
	tb.sendTransform(msg)

# Base_laser to Base_link Callback
def tf_BL_BL_CB(msg):
	tb=tf2_ros.TransformBroadcaster()
	#print("got a BL->BL transform.")
	tb.sendTransform(msg)


def main():
	rospy.init_node('tf_publisher')

	sub1 = rospy.Subscriber('/transformation1', TransformStamped, tf_BL_OD_CB)
	sub2 = rospy.Subscriber('/transformation2', TransformStamped, tf_BL_BL_CB)
	rospy.spin()


if __name__ == '__main__':
    main()

